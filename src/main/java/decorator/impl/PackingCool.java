package decorator.impl;

import decorator.BouquetDecorator;

public class PackingCool extends BouquetDecorator {
    private final double ADDITIONAL_PRICE = 50;
    private final String ADDITIONAL_COMPONENT = "+ Cool packing";

    public PackingCool() {
        setAdditionalPrice(ADDITIONAL_PRICE);
        setAdditionalComponent(ADDITIONAL_COMPONENT);
    }
}

