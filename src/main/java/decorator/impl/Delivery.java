package decorator.impl;


import decorator.BouquetDecorator;

public class Delivery extends BouquetDecorator {

    private final double ADDITIONAL_PRICE = 50;
    private final String ADDITIONAL_COMPONENT = "+ Delivery";

    public Delivery() {
        setAdditionalPrice(ADDITIONAL_PRICE);
        setAdditionalComponent(ADDITIONAL_COMPONENT);
    }
}
