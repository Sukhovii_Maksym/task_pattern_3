package decorator;

public interface BouquetFace {

    double getTotalPrice();

    void print();
}
