package domain;

import java.util.HashMap;
import java.util.Map;

public class FlowerCatalog {
    private Map<Flower, Double> prices = new HashMap<Flower, Double>();

    public FlowerCatalog(Map<Flower, Double> prices) {
        this.prices = prices;
    }

    public FlowerCatalog() {
    }

    public Map<Flower, Double> getPrices() {
        return prices;
    }

    public void setPrices(Map<Flower, Double> prices) {
        this.prices = prices;
    }

    public void addFlower(Flower flower, Double price) {
        prices.put(flower, price);
    }

    public void deleteFlower(Flower flower) {
        prices.remove(flower);
    }

    public Double getFlowerPrice(Flower flower) {
        return prices.get(flower);
    }

    public void printCatalog() {
        for (Map.Entry<Flower, Double> entry : prices.entrySet()) {
            System.out.printf(entry.getKey() + "\t\t<--->\t\t " + entry.getValue() + "uah;\n");
        }
    }
}
