package domain;

public enum Flower {
    Rose, Tulip, Daffodil, Lily, Orchid, Daisy, Iris
}
