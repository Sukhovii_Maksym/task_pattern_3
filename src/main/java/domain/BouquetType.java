package domain;

public enum BouquetType {
    WEDDING, BIRTHDAY, VALENTINE, FUNERAL, NONE
}
